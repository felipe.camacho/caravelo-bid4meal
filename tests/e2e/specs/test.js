// https://docs.cypress.io/api/introduction/api.html

describe('B4M Tests', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('h3', 'Booking info')
  })
})
