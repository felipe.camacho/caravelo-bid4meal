import Vue from 'vue'
import Vuex from 'vuex'
import { shallowMount } from '@vue/test-utils';

import PickerFor_MealBid from '@/components/pickerFor-meal-bid/pickerFor-meal-bid.vue'

Vue.use(Vuex);

const _module_meals = {
  state: {
    all: [
      {
        "mealId":"ML01",
        "desc":"Snacks & Soda",
        "priceRange": {
          "min": 0,
          "max": 20,
          "jump": 5
        },
        "currency": "EUR"
      },
      {
        "mealId":"ML02",
        "desc":"Light Dinner: Salad & Wine",
        "priceRange": {
          "min": 0,
          "max": 50,
          "jump": 10
        },
        "currency": "EUR"
      },
      {
        "mealId":"ML03",
        "desc":"Dinner or Lunch: Meat with Pasta, Salad, Brad rolls, Tiramisu Cake, Cheese and Crackers.",
        "priceRange": {
          "min": 0,
          "max": 100,
          "jump": 25
        },
        "currency": "EUR"
      },
      {
        "mealId":"ML04",
        "desc":"Breackfast: Yogurt, Juice or Cooffe, Bread and Cookies",
        "priceRange": {
          "min": 0,
          "max": 20,
          "jump": 5
        },
        "currency": "EUR"
      }
    ]
  },
  getters: {
    nonSelected_Meals: () => ({mealId}) => {
      return _module_meals.state.all.filter((_item) => {
        return (_item.mealId !== mealId);
      });
    }
  }
};


describe('PickerFor_MealBid', () => {
  let store;
  beforeEach(() => {
    store = new Vuex.Store({
      getters: {
        'meals/nonSelected_Meals': _module_meals.getters.nonSelected_Meals
      }
    });
  });
  it('Meals are printed into buttons', () => {
    const wrapper = shallowMount(PickerFor_MealBid, {
      store
    });
    // wrapper.find('button[data-mealId="ML03"]').trigger('click');
    expect(wrapper.find('button[data-mealId="ML01"]').exists()).toBe(true);
    _module_meals.state.all.forEach((_meal_bid) => {
      const _search_item = `button[data-mealId="${_meal_bid.mealId}"]`;
      expect(wrapper.find(_search_item).exists()).toBe(true);
    });
  })
});

