import { shallowMount } from '@vue/test-utils'
import Passenger from '@/components/passenger/passenger.vue'

const _passenger_data = {
  firstName: 'no-firstName',
  lastName: 'no-lastName',
  prefix: 'no-prefix',
  gender: 'no-gender',
  type: 'no-type'
};

describe('passenger.vue', () => {
  it('renders passenger properties', () => {
    const wrapper = shallowMount(Passenger, {
      propsData: { 
        passenger: _passenger_data
      }
    })
    // see if the data renders
    expect(wrapper.find('span[data-name="name"]').text().trim()).toEqual(`${_passenger_data.firstName} ${_passenger_data.lastName}`);
    expect(wrapper.text().trim().includes(_passenger_data.prefix)).toBe(true);
    expect(wrapper.text().trim().includes(_passenger_data.gender)).toBe(true);
    expect(wrapper.text().trim().includes(_passenger_data.type)).toBe(true);
  })
});
