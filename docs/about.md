# About caravelo-bid4meal
This project is a test for the company "Caravelo"

## Technologies
The main framework used is 'Vue'. For manage application state the option selected is 'Vuex'. For development is also necessary the tool 'VUE-CLI'.

Some other libraries used (included by CDN):
- Bootstrap (CSS and responsive facilities)
- FontAwesome (icons)
- JQuery (used by bootstrap)
- lodash (used for 'debounce' data input)
- moment (used for manage dates)

## Project structure
The project is transpiled and packaged usign 'vue-cli' + 'babel' utilities. Almost all the code is located at 'src' folder.

- **public**

  Contains the 'index.html' and some static files

- **src/api**

  Contains the code for access a REST API. Also the file with the **'mockup-data'**  is included in this path.
  
- **src/components** 

  Contains the different vue components each one inside a folder. 
    - **editorFor-meal-bid** >>> for manage each meal bid. Use of component _pickerFor-meal-bid_ 
    - **mixinFor_FormatDate**  >>> mixing for manage dates
    - **passenger**  >>> shows information for a passenger
    - **pickerFor-meal-bid** >>> menu for made the meal selection. Allows to discard the already selected meal.


- **src/store** 

  Contains the files for manage application state (Vuex). There are two different modules: 'bookings' and 'meals'
  
- **src/views**

  Contains the 'pages' managed by the application router. There are only one page (view).
  
## Project tests
The project contains some test located at the folder 'test'. There are two different kind of test.

- **tests/e2e** >>> E2E tests (cypress)

- **tests/unit** >>> unit tests (jest)




