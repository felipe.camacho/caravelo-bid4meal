import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'


Vue.config.productionTip = false


const _loader = {
  // Load all meals
  _load_All_Meals: async () => {
    try {
      await store.dispatch('meals/getAll_Meals');
    } catch (_e) {
      // TODO: handle exception
    }
  },
  // Load data
  load_DATA: async () => {
    await _loader._load_All_Meals();
  }
};

_loader.load_DATA();


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
