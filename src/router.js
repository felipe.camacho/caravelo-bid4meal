import Vue from 'vue'
import Router from 'vue-router'

import B4M_Request from './views/b4m-request/b4m-request.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'B4M_Request',
      component: B4M_Request
    }
  ]
})
