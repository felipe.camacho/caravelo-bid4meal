'use strict'

import apiFor_Bookings from '../../api/apiFor-bookings.js';

// initial state
const state = {
  all: [],
  current: {}
}

// getters
const getters = {
};

// actions
const actions = {  
  get_Booking ({commit}, {booking_id}) {
    return new Promise((_resolve, _reject) => {
      apiFor_Bookings.get_Booking({booking_id}).then(_response => {
        commit('set_Booking', _response);
        _resolve(_response);
      },
      _error => {
        // manage Error
        _reject(_error);
      });
    });
  }
};

// mutations
const mutations = {
  set_Booking (state, _booking) {
    state.current = _booking;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
