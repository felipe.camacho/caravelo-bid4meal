'use strict'

import apiFor_Meals from '../../api/apiFor-meals.js';

/*
{
  "selection": [
    {
      "journeyKey": "TST12 BCN-LAX 30/08/2014 08:35",
      "amount": 15,
      "currency": "EUR",
      "mealId": "ML03"
    },
    {
      "journeyKey": "TST21 LAX-BCN 30/09/2014 08:35",
      "amount": 10,
      "currency": "EUR",
      "mealId": "ML01"
    }
  ]
}
*/

// initial state
const state = {
  all: [],
  selection: []
}

// getters
const getters = {
  nonSelected_Meals: (state) => ({mealId}) => {
    return state.all.filter((_item) => {
      return (_item.mealId !== mealId);
    });
  }
};

// actions
const actions = {  
  getAll_Meals ({commit}) {
    return new Promise((_resolve, _reject) => {
      apiFor_Meals.get_Meals().then(_response => {
        commit('set_Meals', _response);
        _resolve(_response);
      },
      _error => {
        // manage Error
        _reject(_error);
      });
    });
  }
};

// mutations
const mutations = {
  set_Meals (state, _meals) {
    state.all = _meals;
  },
  add_MealBid (state, _mealBid) { // not used, for the test is not necessary
    state.current = state.current.filter((_item) => {
      return (_item.journeyKey !== _mealBid.journeyKey)
    });
    state.current.push(_mealBid);
  },
  update_MealBid (state, _mealBid) {  // not used, for the test is not necessary
    state.current = [
      ...state.current.filter((_item) => {
        return (_item.journeyKey !== _mealBid.journeyKey);
      }),
      _mealBid
    ];
  },
  remove_MealBid (state, _mealBid) {  // not used, for the test is not necessary
    state.current = state.current.filter((_item) => {
      return (_item.journeyKey !== _mealBid.journeyKey);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
