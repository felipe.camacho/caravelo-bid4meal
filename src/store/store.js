import Vue from 'vue'
import Vuex from 'vuex'

import bookings from './modules/bookings.js';
import meals from './modules/meals.js';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    bookings,
    meals
  }
})
