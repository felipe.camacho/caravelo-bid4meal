"use strict"

/*global jQuery*/
/*eslint no-unused-vars: ["error", { "argsIgnorePattern": "^_" }]*/

import Component_Passenger from '../../components/passenger/passenger.vue';
import Component_EditorFor_MealBid from '../../components/editorFor-meal-bid/editorFor-meal-bid.vue';

import mixinFor_formatDate from '../../components/mixinFor_FormatDate/mixinFor_FormatDate.js';

/**
 * Page Bid4Meal
 * This is the main (and the only) page of the project
 */
const Page_B4M_Request = {

  components: {
    'passenger-info': Component_Passenger,
    'editorFor_MealBid': Component_EditorFor_MealBid
  },
  
  mixins: [mixinFor_formatDate],

  data () {
    return {
      title: 'Page_B4M_Request',
      booking_id: null,
      booking: {},
      meals_bids: [],
      continue_enabled: false,
      selection_data: ''
    };
  },
 
  
  watch: {
    meals_bids: function (_new_val, _old_val) { // Check when any meal bid has amount bigger than 0... then 'continue' is enabled
      const _meals_bids_with_values = _new_val.filter((_meal) => {
        return _meal.amount > 0;
      });
      if (_meals_bids_with_values.length > 0) {
        this.continue_enabled = true;
      } else {
        this.continue_enabled = false;
      }
    }
  },
  
  
  props: {
  },
   
  methods: {

    setData({id, booking}) {
      this.booking_id = id || this.booking_id;
      this.booking = booking || this.booking;
    },
    
    _handle_mealAmountUpdated: function(_item, _event) {
      this.meals_bids = [ // update the meal
        ...this.meals_bids.filter((_meal_bid) => {
          return _meal_bid.journeyKey !== _item.journeyKey;
        }),
        _item
      ];
    },
    
    _handle_mealRemoved: function(_item, _event) {
      this.meals_bids = this.meals_bids.filter((_meal_bid) => { // remove the meal
        return _meal_bid.journeyKey !== _item.journeyKey;
      });
    },
    
    _handle_mealSelected: function(_item, _event) {
      this.meals_bids = [ // Add the meal
        ...this.meals_bids.filter((_meal_bid) => {
          return _meal_bid.journeyKey !== _item.journeyKey;
        }),
        _item
      ];
    },
    
    _continue: function() {
      console.info('_continue');
      const _selection_data = {
        selection: []
      };
      const _meals_bids_with_values = this.meals_bids.filter((_meal_bid) => { // find bids with values
        return _meal_bid.amount > 0;
      });
      _selection_data.selection = _meals_bids_with_values.map((_meal_bid) => {  // prepare data
        return {
          'journeyKey': _meal_bid.journeyKey,
          'amount': _meal_bid.amount,
          'currency': _meal_bid.meal.currency,
          'mealId': _meal_bid.meal.mealId
        };
      });
      this.selection_data = JSON.stringify(_selection_data, null, 2); // JSON with spacing level = 2
      const _JQ = jQuery;
      const _modal = _JQ(this.$el).find('#modalFor_Selecion')[0];
      _JQ(_modal).modal('show'); // open modal using jQuery
      
    }
    
  },
  
  // Method for manage route options
  beforeRouteEnter (to, from, next) {
    next( async (vm) => {
      const booking_id = to.params.id;
      const booking = await vm.$store.dispatch('bookings/get_Booking', {booking_id});
      vm.setData({
        id: booking_id,
        booking
      });
    });
  },
  
  // when route changes and this component is already rendered,
  // the logic will be slightly different.
  beforeRouteUpdate (to, from, next) {
    this.booking = null
    const booking_id = to.params.id;
    this.$store.dispatch('bookings/get_Booking', {booking_id}).then((booking) => {
      this.setData({
        id: booking_id,
        booking
      });
      next();
    }, (_error) => {
      console.log(_error);  // TODO: Manage error
      next();
    });
  }
    
};

export default Page_B4M_Request;
