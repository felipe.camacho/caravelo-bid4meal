/*global moment*/

/**
 * Mixin for print date more friendly
 */
const formatDateMixin = {
  methods: {
    formatDate: (value) => {
      if( !value) return '';

      return moment(value).format('YYYY-MM-DD HH:mm:ss');
    },
    formatDay: (value) => {
      if( !value) return '';

      return moment(value).format('YYYY-MM-DD');
    },
    formatHour: (value) => {
      if( !value) return '';

      return moment(value).format('HH:mm:ss');
    }
  }
};

export default formatDateMixin;
