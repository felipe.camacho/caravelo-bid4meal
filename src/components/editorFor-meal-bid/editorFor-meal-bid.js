"use strict"

/*global _*/
/*eslint no-unused-vars: ["error", { "argsIgnorePattern": "^_" }]*/

import Component_PickerFor_Meal from '../pickerFor-meal-bid/pickerFor-meal-bid.vue';

/**
 * Component for manage the bid for a meal.
 * Make use of the component 'pickerFor-meal-bid'
 */
const Component_EditorFor_MealBid = {

  components: {
    'pickerFor-meal': Component_PickerFor_Meal
  },


  data () {
    return {
      amount: 0,
      meal: {},
      meal_selected: false
    };
  },
  
  watch: {
    amount: function (_new_val, _old_val) { // Check correct value for amount
      const _amount = parseFloat(_new_val);
      this.amount = (!isNaN(_amount)) ? _amount : 0;
      this._debounced_NotifyAmount(); // Notify change with 'debounce' behaviour
    }
  },
  
  props: {
    journeyKey: {
      type: String
    }
  },
  
  methods: {
    _handle_itemSelected: function(_item, _event) {
      this.meal = _item;
      this.amount = 0;
      this.meal_selected = true;
      this.$emit('selected:item', { // emit event 'selected:item'
        meal: this.meal,
        amount: this.amount,
        journeyKey: this.journeyKey
      });
    },
    _showPickerFor_Meals: function() {
      this.$refs.pickerFor_meal.show_modal();
    },
    _remove_Meal: function() {
      this.meal = {};
      this.meal_selected = false;
      this.$emit('removed:item', {  // emit event 'removed:item'
        journeyKey: this.journeyKey
      });
    },
    _notifyAmount: function() {
      this.$emit('updated:amount', {  // emit event 'updated:amount'
        meal: this.meal,
        amount: this.amount,
        journeyKey: this.journeyKey
      });
    }
  },
  
  created: function () {
    this._debounced_NotifyAmount = _.debounce(this._notifyAmount, 500); // Create a method with 'debounce' behavior
  }
    
};


export default Component_EditorFor_MealBid;
