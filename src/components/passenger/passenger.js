"use strict"

/**
 * Component for show the passenger data
 */
const Component_Passenger = {
    
  data () {
    return {};
  },
  
  props: {
    
    passenger: {
      type: Object,
      default: function () {
        return {
          firstName: 'no-firstName',
          lastName: 'no-lastName',
          prefix: 'no-prefix',
          gender: 'no-gender',
          type: 'no-type'
        };
      }
  
    }
    
  }
    
};


export default Component_Passenger;
