'use strict'

/*global jQuery*/
/*eslint no-unused-vars: ["error", { "argsIgnorePattern": "^_" }]*/

const _JQ = jQuery;

/**
 * Component for make de meal selection
 */
const Component_PickerFor_Meal = {

    components : {},

    data () {
      return {}
    },
    
    props: {
      'meal_selected': ''
    },
    
    computed: {

      meals: function() {
        return this.$store.getters['meals/nonSelected_Meals']({ // Get the meals without the already selected one
          mealId: this.meal_selected
        });
      }

    },
    
    methods: {
      
      show_modal: function() {
        const _modal = _JQ(this.$el).find('#modalFor_Picker')[0];
        _JQ(_modal).modal('show'); // open modal using jQuery
      },
      
      _handleSelectItemClick(_item, _event) {
        const _modal = _JQ(this.$el).find('#modalFor_Picker')[0];
        _JQ(_modal).modal('hide'); // close modal using jQuery
        this.$emit('selected:item', _item); // Emit the event with the selected item
      }
    
    }
    
};


export default Component_PickerFor_Meal;
