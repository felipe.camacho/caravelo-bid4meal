'use strict'

import mockupDATA from './mockup-data.js';

/*
 * Library for manage REST API caalls related to bookings
 */
const apiForBookins = {

  get_Booking: ({booking_id=0}) => {
    console.info('get_Booking', booking_id);  // TODO: REMOVE DEBUG LOG
    return new Promise((_resolve, _reject) => {
      if (mockupDATA === undefined) {_reject('No mockup data');}
      _resolve(mockupDATA.booking);
    });
  }

}

export default apiForBookins;
