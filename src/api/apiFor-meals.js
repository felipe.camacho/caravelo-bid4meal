'use strict'

import mockupDATA from './mockup-data.js';

/*
 * Library for manage REST API caalls related to meals
 */
const apiForMeals = {

  get_Meals: () => {
    return new Promise((_resolve, _reject) => {
      console.log('apiForMeals.get_Meals');
      console.log(mockupDATA.options);
      if (mockupDATA === undefined) {_reject('No mockup data.');}
      _resolve(mockupDATA.options);
    });
  }

}

export default apiForMeals;
